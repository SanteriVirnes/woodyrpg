﻿using UnityEngine;
using System.Collections;

public class BatScript : MonoBehaviour {
    
    public Transform target;
    public bool attack = false;
    public float speed = 10.0f;
    float currentSpeed = 0.0f;
    public float minSpeed = 3.0f;

	public enum ActionNPC{Attack, DamageSpell, HealSpell};

	public int hitpoints;
	public int criticalLimit;
	
	public int magicPoints;
	public int damageSpellCost;
	public int healSpellCost;

	public int attackPower;
	public int damageSpellPower;
	public int healPower;

    public bool IsSelected { 
        get {
            return transform.GetChild(0).gameObject.renderer.enabled;
        }
        set
        { // enables / disables quad renderer (displays circle)
            transform.GetChild(0).gameObject.renderer.enabled = value;
        }
        
    }
	// Use this for initialization
	void Start () 
    {
        this.transform.parent = GameObject.Find("BatSpawnPoint").transform;
	}

	public ActionNPC getAction()
	{
		if (hitpoints > criticalLimit) {
			if (magicPoints > damageSpellCost)
				return ActionNPC.DamageSpell;
			else
				return ActionNPC.Attack;
		} else if (magicPoints > healSpellCost)
			return ActionNPC.HealSpell;
		else 
			return ActionNPC.Attack;
		
	}

	
	// Update is called once per frame
	void Update () 
    {
        if (attack)
        {
            if (IsSelected)
            {
                Destroy(this.gameObject);
            }
            else
            {
                float distance = Vector3.Distance(transform.position, target.position);
                currentSpeed = speed / (distance * distance * distance);
                transform.position = Vector3.MoveTowards(transform.position, target.position, currentSpeed * Time.deltaTime);
            }

        }
       
	}

	public void DestroyMe()
	{
		Destroy(this.gameObject);
	}

    public void Attack()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        attack = true;
    }
     
  
    void OnTriggerEnter(Collider other)
    {
        Debug.Log("TriggerEnter");
        if (other.tag == "Player")
        {
            Debug.Log("Sending hit");
            other.gameObject.GetComponent<ClickMove>();
            Destroy(this.gameObject);
        }
    }
}
