﻿using UnityEngine;
using System.Collections;

public class RiseScript : MonoBehaviour
{

    public Transform risePosition;
    public Transform fallPosition;
    public Transform targetObject;
    public static bool gameOn = false;

    enum Action { Rise, Fall, Idle };
    Action action = Action.Idle;

    public void Rise()
    {
        action = Action.Rise;
    }
    public void Fall()
    {
        action = Action.Fall;
    }

    // Update is called once per frame
    public void Update()
    {
        Vector3 targetPosition = Vector3.zero;
        switch (action)
        {
            case Action.Rise:
                targetPosition = risePosition.position;
                break;
            case Action.Fall:
                targetPosition = fallPosition.position;
                break;
        }
        if (action != Action.Idle)
        {
            targetObject.position = Vector3.MoveTowards(targetObject.position,
                                    targetPosition, Vector3.Distance
                                    (targetObject.position, targetPosition) * 0.1f);
            if (Vector3.Distance(targetObject.position, targetPosition) < 0.01f)
            {
                action = Action.Idle;
            }
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameObject.Find("Peasant 1(Clone)").GetComponent<ClickMove>().ableToMove = false;
            Rise();
            gameOn = true;
        }

    }
}
