﻿using UnityEngine;
using System.Collections;

public class BatAttackController : MonoBehaviour {

	PlayerController playerController;
	BatScript npcController;
	
	bool playerturn;
	bool battleOver;
	bool battleStarted;
	
	bool npcSelected;
	
	public int damageModifierUpperLimit;
	public int healModifierUpperLimit;

	int damageModifier;
	int healModifier;

    //float passedTime = 0.0f;
    public float batWaveInterval = 15.0f; /// How long to wait before creating another wave.
    public float batSpawningInterval = 0.5f; /// Time between bat instancing.
    public float batAttackHoldTime = 1.0f; /// Time to wait before attack begins after all bats in wave are in place.
    
    public int[] batCount = new int[]{5}; /// How many bats to create within a wave. Default five bats, one wave.
    public int currentWave = 0;                                          
    public float radius = 4.0f; /// Radius to spread bats around batSpawnPoint
    
    public Transform batSpawnPoint = null;
    public Transform batPrefab;
    public bool createWave = true;
    //bool   shootBatsAvailable = false;
    
	public void Awake()
    {
        if (batSpawnPoint == null)
            batSpawnPoint = this.transform;

		CreateBats();

		playerturn = true;
		battleOver = false;
		battleStarted = false;
		
		npcSelected =false;
    }

	public void Start()
	{
		playerController = (GameObject.FindWithTag ("Player").GetComponent ("PlayerController")) as PlayerController;
	}
    

	void Update () 
	{
		if (!battleOver)
		{
			damageModifier = Random.Range(0,damageModifierUpperLimit);
			healModifier = Random.Range(0,healModifierUpperLimit);

			Transform t = transform.GetChild(0);
			npcController = t.gameObject.GetComponent<BatScript>();

			if (playerturn) {
				PlayerController.ActionPlayer currentAction = playerController.getAction();
				if (currentAction != PlayerController.ActionPlayer.notYetSelected) {
					executePlayerAction (currentAction);
				if (!battleStarted)
						battleStarted = true;
				}
			} else {
				if (npcSelected == false)
					StartCoroutine(NPCActionStarter());
				
			}
		}


	}

	void OnGUI()
	{
		if (battleStarted && !battleOver) {
			GUI.contentColor = Color.black;
			GUI.Label (new Rect (Screen.width/2 - 100, 0, 200, 100), "A horde of bats attacks you. Battle on!");
		}
	}

	void executePlayerAction(PlayerController.ActionPlayer actionToExecute)
	{
		Debug.Log ("Player acted: " + actionToExecute);
		
		switch (actionToExecute)
		{
		case PlayerController.ActionPlayer.Attack:
			npcController.hitpoints -= playerController.attackPower + damageModifier;
			playerturn = false;
			break;
		case PlayerController.ActionPlayer.DamageSpell:
			if (playerController.magicPoints >= playerController.damageSpellCost)
			{
				npcController.hitpoints -= playerController.damageSpellPower + damageModifier;
				playerController.magicPoints -= playerController.damageSpellCost;
				playerturn = false;
			}
			else
				Debug.Log("Not enough magic points, select another action");
			break;
		case PlayerController.ActionPlayer.HealSpell:
			if (playerController.magicPoints >= playerController.healSpellCost)
			{
				playerController.hitpoints += playerController.healPower + healModifier;
				playerController.magicPoints -= playerController.healSpellCost;
				playerturn = false;
			}
			else
				Debug.Log("Not enough magic points, select another action");
			break;
		default:
			break;
		}
		
		if (npcController.hitpoints <= 0)
		{
			Debug.Log ("NPC died");
			npcController.DestroyMe();
			battleOver = true;
		}
		else
		{
			Debug.Log ("Player hitpoints: " + playerController.hitpoints);
			Debug.Log ("NPC hitpoints: " + npcController.hitpoints);
		}
		
		npcSelected = false;
	}
	
	void executeNPCAction(BatScript.ActionNPC actionToExecute)
	{
		Debug.Log ("NPC acted: " + actionToExecute);
		
		switch (actionToExecute)
		{
		case BatScript.ActionNPC.Attack:
			playerController.hitpoints -= npcController.attackPower + damageModifier;
			playerturn = true;
			break;
		case BatScript.ActionNPC.DamageSpell:
			if (npcController.magicPoints >= npcController.damageSpellCost)
			{
				playerController.hitpoints -= npcController.damageSpellPower + damageModifier;
				npcController.magicPoints -= npcController.damageSpellCost;
				playerturn = true;
			}
			else
				Debug.Log("Not enough magic points, select another action");
			break;
		case BatScript.ActionNPC.HealSpell:
			if (npcController.magicPoints >= npcController.healSpellCost)
			{
				npcController.hitpoints += npcController.healPower + healModifier;
				npcController.magicPoints -= npcController.healSpellCost;
				playerturn = true;
			}
			else
				Debug.Log("Not enough magic points, select another action");
			break;
		default:
			break;
		}
		
		if (playerController.hitpoints <= 0)
		{
			Debug.Log ("Player died");
			battleOver = true;

		}
		
		
		
	}
	
	IEnumerator NPCActionStarter() {
		npcSelected = true;
		print("NPC ponders carefully its counter move ...");
		yield return new WaitForSeconds(1);
		BatScript.ActionNPC currentAction = npcController.getAction ();
		executeNPCAction (currentAction);
	}

	public void CreateBats()
	{
		float angleStep = Mathf.PI / batCount[0];
		float angle = -Mathf.PI*0.5f;
		Debug.Log("created a bat"); 
		for (int i = 0; i < batCount[0]; i++)
		{
			Debug.Log("created a bat");  
			Vector3 pos = batSpawnPoint.transform.position;
			pos.x += Mathf.Sin(angle) * radius;
			pos.y += Mathf.Cos(angle) * radius;
			GameObject obj = GameObject.Instantiate(batPrefab, pos, Quaternion.AngleAxis(180.0f,Vector3.up)) as GameObject;
			angle += angleStep;
		}
	}
}