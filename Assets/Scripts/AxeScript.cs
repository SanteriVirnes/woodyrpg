﻿using UnityEngine;
using System.Collections;

public class AxeScript : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        GameObject[] Tag = GameObject.FindGameObjectsWithTag("Axe");
        int AxeCount = Tag.Length;
        if (AxeCount >= 1)
        {
            Destroy(this.gameObject);
        }
    }
}
