﻿using UnityEngine;
using System.Collections;

public class DeathAreaScript : MonoBehaviour
{

    private GameObject MainCamera;

    // Use this for initialization
    void Start()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            MainCamera = GameObject.Find("Main Camera");
            Destroy(MainCamera);
            Destroy(other.gameObject);

            Application.LoadLevel("DeathScene");
        }


    }
    // Update is called once per frame
    void Update()
    {


    }
}
